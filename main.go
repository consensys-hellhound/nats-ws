package main

import (
	"log"
	"os"
)

func main() {
	natsUrl, ok := os.LookupEnv("NATS_URL")
	if !ok {
		log.Fatal("cannot start without NATS_URL")
	}

	relay := NewRelay(
		natsUrl,
		BinaryMode(false),
		ListenPort(4223),
	)
	errors := make(chan error)
	go relay.Do(errors)
	err := <-errors
	log.Fatal(err.Error())
}
