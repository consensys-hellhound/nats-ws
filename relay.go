package main

import (
	"fmt"
	"golang.org/x/net/websocket"
	"io"
	"log"
	"net"
	"net/http"
)

var (
	DefaultOptions = &Options{
		BinaryMode: true,
		ListenPort: 4223,
	}
)

type Relay struct {
	target string
	opts   Options
}

func NewRelay(target string, setters ...Option) Relay {
	opts := DefaultOptions
	for _, setter := range setters {
		setter(opts)
	}
	relay := Relay{
		target: target,
		opts:   *opts,
	}
	return relay
}

func (relay Relay) Do(errors chan<- error) {

	portString := fmt.Sprintf(":%d", relay.opts.ListenPort)
	log.Printf("[INFO] Listening on %s\n", portString)

	http.Handle("/", websocket.Handler(relay.Handler))
	errors <- http.ListenAndServe(portString, nil)
}

type Option func(*Options)

type Options struct {
	BinaryMode bool
	ListenPort int
}

func BinaryMode(binaryMode bool) Option {
	return func(options *Options) {
		options.BinaryMode = binaryMode
	}
}

func ListenPort(port int) Option {
	return func(options *Options) {
		options.ListenPort = port
	}
}

func copyWorker(dst io.Writer, src io.Reader, doneCh chan<- bool) {
	io.Copy(dst, src)
	doneCh <- true
}

func (relay Relay) Handler(ws *websocket.Conn) {
	log.Println("connecting to : ", relay.target)
	conn, err := net.Dial("tcp", relay.target)
	if err != nil {
		log.Printf("[ERROR] %v \n", err)
		return
	}

	if relay.opts.BinaryMode {
		ws.PayloadType = websocket.BinaryFrame
	}

	doneCh := make(chan bool)

	go copyWorker(conn, ws, doneCh)
	go copyWorker(ws, conn, doneCh)

	<-doneCh
	conn.Close()
	ws.Close()
	<-doneCh
}
