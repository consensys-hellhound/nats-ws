# build stage
FROM golang:1.11-alpine AS build-stage
RUN apk update
RUN apk add --no-cache git gcc musl-dev
WORKDIR /bin
COPY . .
RUN go build -o /relay


# final stage
FROM alpine
COPY --from=build-stage /relay /relay
EXPOSE 4223
CMD ["/relay"]
